import java.util.*;

public class Cell {
    public int value;               //1-9
    public int column;              //1-9
    public int row;                 //1-9
    public int subgrid;             //1-9
    public int[] possibleValues;    //123456789
    public int numPossibilities;    //9 -> 1
    public boolean fixed;
    public boolean searched;

    
    //Constructor for creating deep copy of cells.
    public Cell(int row, int column, int subgrid, int value, int[] possibleValues, int numPossibilities, boolean fixed, boolean searched){
        this.row = row;
        this.column = column;
        this.subgrid = subgrid;
        this.value = value;
        this.possibleValues = possibleValues.clone();
        this.numPossibilities = numPossibilities;
        this.fixed = fixed;
        this.searched = searched;
    }

    //Constructor for creating newly unfixed cells
    public Cell(int row, int column){
        this.row = row;
        this.column = column;
        this.subgrid = determineSubgrid(row, column);
        this.possibleValues = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.numPossibilities = 9;
        this.fixed = false;
        this.value = 0;
    }

    //Constructor for initially fixed cells
    public Cell(int row, int column, int value){
        this.row = row;
        this.column = column;
        this.subgrid = determineSubgrid(row, column);
        this.possibleValues = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        this.possibleValues[value-1] = value;
        this.numPossibilities = 1;
        this.fixed = true;
        this.value = value;
    }


    //Determines subgrid of cell based on row & column
    private int determineSubgrid(int row, int column){
        if(row == 1 || row == 2 || row == 3){
            if(column == 1 || column == 2 || column == 3){
                return 1;
            }
            if(column == 4 || column == 5 || column == 6){
                return 2;
            }
            if(column == 7 || column == 8 || column == 9){
                return 3;
            }
        }
        if(row == 4 || row == 5 || row == 6){
            if(column == 1 || column == 2 || column == 3){
                return 4;
            }
            if(column == 4 || column == 5 || column == 6){
                return 5;
            }
            if(column == 7 || column == 8 || column == 9){
                return 6;
            }
        }
        if(row == 7 || row == 8 || row == 9){
            if(column == 1 || column == 2 || column == 3){
                return 7;
            }
            if(column == 4 || column == 5 || column == 6){
                return 8;
            }
            if(column == 7 || column == 8 || column == 9){
                return 9;
            }
        }
        System.out.println("Invalid row and/or column index");
        System.exit(0);
        return 0;
    }


    //DEBUG: printing cell data
    public void printCellData(){
        if(this.fixed == true) {
            System.out.println("FIXED");
        }
        System.out.println("[" + this.row + "]" + "[" + this.column + "] = " + this.value + "(" + this.numPossibilities + ")");
        for(int i = 0; i < possibleValues.length; i++) {
            System.out.print(possibleValues[i]);
        }

        System.out.print("\n\n");
    }
    
    //DEBUG: for printing cell data
    public String returnCellData(){
        String updateString = "\n";
        if(this.fixed == true) {
            updateString += "FIXED \n";
        }
        updateString += "[" + this.row + "]" + "[" + this.column + "] = " + this.value + "(" + this.numPossibilities + ")" + "\n";
        for(int i = 0; i < possibleValues.length; i++) {
            updateString += possibleValues[i];
        }
        updateString += "\n\n";
        return updateString;
    }
}
