# Sudoku Solver

This program solves a sodoku puzzle using the backtracking technique. The backtracking technique is a method used to exhaustively search a solution space. In short, the Sudoku solver uses backtracking to build potential solutions by creating copies of the board, systematically “guessing” values of cells in that copy, and abandoning the copy and "backtracking" as soon as a solution is determined to be infeasible.


Usage: Java Solver infile.txt outfile.txt 

*For infile, select from p1.txt through p7.txt*


The puzzle is represented by a two-dimensional array of Cell objects. Each Cell object stores the cell row, column, subgrid, value, a list of possible values, number of possible values, a fixed flag, and a searched flag. The infiles contain a list of [row column value] parameters that describe the initial configuration of the board. After the initial configuration is established, the board is updated so that all cells in the same row, column and subgrid of the fixed cell do not have the fixed cell's value in their list of possible values. When a cell has only one possible value in its list of possibilities, its value is fixed.

After the initial configuration of the board has been established and appropriate board updates have been performed, the recursive backtracking begins. The recursive backtracking technique operates as follows: 

* Select the cell with the lowest number of possible values. 
* For each possible value of that cell, create a copy of the board and set its value to one of the possibilities. In essence, "guess" the value of that cell in the final board solution. 
* Update the board copy to reflect the newly fixed cell. That is, remove value of the newly fixed cell from the possibilities list of each cell sharing a row/column/subgrid of the fixed cell. If the update caused any cells to have only one possibility left in its list of possibilities, fix that cell and perform subsequent updates. The update function also checks if only one cell in a given row/column/subgrid allows a particular value - if so, that cell's value is fixed. 
* Continue solving using this method on the board copy until a solution in this board copy is determined to be infeasible (at least one cell has 0 possible values), or a correct solution is found (all cells have only 1 possible value).

When a solution is found, the puzzle solution is printed to a file row by row along with the number of board configurations that were attempted (the number of recursive calls made in the backtracking algorithm).