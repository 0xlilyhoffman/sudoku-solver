public class Board {
    public Cell[][] board;

    public Board(){
        this.board = new Cell[9][9];
    }

    //NOTE: board is indexed 0-8. Cells are identified 1-9
    public Board initializeBoardConfiguration(int[][] initialCells){
        //Initialize blank cells
        for(int r = 0; r < 9; r++){
            for(int c = 0; c < 9; c++){
                this.board[r][c] = new Cell(r+1, c+1);
            }
        }

        //Fill in fixed cells
        for(int i = 0; i < initialCells.length; i++){
            int fixedRow = initialCells[i][0];
            int fixedColumn = initialCells[i][1];
            int fixedValue = initialCells[i][2];
            this.board[fixedRow-1][fixedColumn-1] = new Cell(fixedRow, fixedColumn, fixedValue);
        }

        //Return board
        return this;
    }

    //Perform deep copy of all cells in board
    public Board copyBoard(Board b){
        Board bcopy = new Board();
        for(int r = 0; r < 9; r++){
            for(int c = 0; c < 9; c++){
                Cell cellToCopy = b.board[r][c];
                Cell cellCopy = new Cell(cellToCopy.row, cellToCopy.column, cellToCopy.subgrid, cellToCopy.value, cellToCopy.possibleValues, cellToCopy.numPossibilities, cellToCopy.fixed, cellToCopy.searched);
                bcopy.board[r][c] = cellCopy;
            }
        }
        return bcopy;
    }

    //for every fixed cell value, update list of possible values for each cell in same row/col/subgrid
    public Board updateBoard(){

        //OPTIMIZATION 1:
        //Scan entire board
        //When a fixed cell is found,remove value of fixed cell from possibilities list of each shared row/col/subgrid
        for(int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                if(this.board[r][c].value != 0){
                    Cell fixedCell = this.board[r][c];
                    updateRow(fixedCell.value, r, c);
                    updateColumn(fixedCell.value, r, c);
                    updateSubgrid(fixedCell.value, fixedCell.subgrid, r, c);
                }
            }
        }

        //OPTIMIZATION 2:
        //For each row/col/subgrid, check if only one cell contains value "v" in its possibilities list - if so, set it
        for(int i = 0; i < 9; i++){
            checkRowExclusion(i);
            checkColExclusion(i);
            checkSubgridExclusion(i);
        }
        return this;
    }


    //Checks if there is only one possibility left for that cell and fixes that cell's value
    //After fixing a cell's value, subsequent update row/col/subgrid must be performed
    public void checkFixedValue(int r, int c){
        //If one possibility left for cell
        if(this.board[r][c].numPossibilities == 1){

            //find possible value and set it
            for(int i = 0; i < 9; i ++){
                if(this.board[r][c].possibleValues[i] != 0){
                    this.board[r][c].value = i+1;

                    //Update row/col/subgrid to reflect fixed cell
                    Cell fixedCell = this.board[r][c];
                    updateRow(fixedCell.value, r, c);
                    updateColumn(fixedCell.value, r, c);
                    updateSubgrid(fixedCell.value, fixedCell.subgrid, r,c );

                }
            }
        }
    }

    //Removes value of fixed cell from shared row
    //@param: fixedRow and fixedColumn are zero-indexed row/col of fixed cell
    public void updateRow(int fixedCellValue, int fixedRow, int fixedColumn){
        for(int i = 0; i < 9; i++){                                                     //scan row with fixed cell
            if(i == fixedColumn){
                int shutupintellij = 1;
                continue;
            }
            if(this.board[fixedRow][i].possibleValues[fixedCellValue - 1] != 0) {   //if the newly fixed cell value is present in list of possibilities
                this.board[fixedRow][i].possibleValues[fixedCellValue - 1] = 0;     //remove it
                this.board[fixedRow][i].numPossibilities--;                         //decrement number of possibilities for that cell
                checkFixedValue(fixedRow, i);                                       //check if there is now only one possibility left for that cell and sets it
            }
        }
    }
    
    //Remove value of fixed cell from shared column
    //@param: fixedRow and fixedColumn are zero-indexed row/col of fixed cell
    public void updateColumn(int fixedCellValue, int fixedRow, int fixedColumn){
        for(int i = 0; i < 9; i++){
            if(i == fixedRow){
                continue;
            }
            if(this.board[i][fixedColumn].possibleValues[fixedCellValue - 1] != 0) {
                this.board[i][fixedColumn].possibleValues[fixedCellValue - 1] = 0;
                this.board[i][fixedColumn].numPossibilities--;
                checkFixedValue(i, fixedColumn);
            }
        }
    }


    //Remove value of fixed cell from shared subgrid
    public void updateSubgrid(int fixedCellValue, int fixedCellSubgrid, int fixedRow, int fixedColumn){
        int subgridRowStart = 0;
        int subgridRowEnd = 0;
        int subgridColStart = 0;
        int subgridColEnd = 0;


        //Determine subgrid index bounds in board
        switch (fixedCellSubgrid){
            case 1:
                subgridRowStart = 0;
                subgridRowEnd = 3;
                subgridColStart = 0;
                subgridColEnd = 3;
                break;
            case 2:
                subgridRowStart = 0;
                subgridRowEnd = 3;
                subgridColStart = 3;
                subgridColEnd = 6;
                break;
            case 3:
                subgridRowStart = 0;
                subgridRowEnd = 3;
                subgridColStart = 6;
                subgridColEnd = 9;
                break;
            case 4:
                subgridRowStart = 3;
                subgridRowEnd = 6;
                subgridColStart = 0;
                subgridColEnd = 3;
                break;
            case 5:
                subgridRowStart = 3;
                subgridRowEnd = 6;
                subgridColStart = 3;
                subgridColEnd = 6;
                break;
            case 6:
                subgridRowStart = 3;
                subgridRowEnd = 6;
                subgridColStart = 6;
                subgridColEnd = 9;
                break;
            case 7:
                subgridRowStart = 6;
                subgridRowEnd = 9;
                subgridColStart = 0;
                subgridColEnd = 3;
                break;
            case 8:
                subgridRowStart = 6;
                subgridRowEnd = 9;
                subgridColStart = 3;
                subgridColEnd = 6;
                break;
            case 9:
                subgridRowStart = 6;
                subgridRowEnd = 9;
                subgridColStart = 6;
                subgridColEnd = 9;
                break;
        }


        //Scan subgrid
        for(int r = subgridRowStart; r < subgridRowEnd; r++){
            for(int c = subgridColStart; c < subgridColEnd; c++){
                if(r == fixedRow && c == fixedColumn){
                    continue;
                }
                if(this.board[r][c].possibleValues[fixedCellValue-1] != 0) {
                    this.board[r][c].possibleValues[fixedCellValue - 1] = 0;
                    this.board[r][c].numPossibilities--;

                    //If only one possibility left, set it
                    checkFixedValue(r, c);
                }
            }
        }
    }


    //for each row
    //if only one cell contains value v in its possibilities list, set it
    public void checkRowExclusion(int row) {
        int cellsInRowAllowingValue = 0;
        int rowToSet = -1;
        int colToSet = -1;

        for (int value = 1; value <= 9; value++) {                                      //for all possible values
            for (int col = 0; col < 9; col++) {                                         //scan each column in row
                if(this.board[row][col].possibleValues[value-1] == value){              //if cell allows "value"
                    cellsInRowAllowingValue++;
                    rowToSet = row;
                    colToSet = col;
                }
            }

            //if there is only one cell in that row that allows "value", set it
            if(cellsInRowAllowingValue == 1){
                this.board[rowToSet][colToSet].value = value;
                updateRow(value, rowToSet, colToSet);
                updateColumn(value, rowToSet, colToSet);
                updateSubgrid(value, this.board[rowToSet][colToSet].subgrid, rowToSet, colToSet);
            }
            cellsInRowAllowingValue = 0;
        }
    }

    //for each row
    //if only one cell contains value v in its possibilities list, set it
    public void checkColExclusion(int col) {
        int cellsInRowAllowingValue = 0;
        int rowToSet = -1;
        int colToSet = -1;

        for (int value = 1; value <= 9; value++) {                                      //for all possible values
            for (int row = 0; row < 9; row++) {                                         //scan each column in row
                if(this.board[row][col].possibleValues[value-1] == value){              //if cell allows "value"
                    cellsInRowAllowingValue++;
                    rowToSet = row;
                    colToSet = col;
                }
            }

            //if there is only one cell in that row that allows "value", set it
            if(cellsInRowAllowingValue == 1){
                this.board[rowToSet][colToSet].value = value;
                updateRow(value, rowToSet, colToSet);
                updateColumn(value, rowToSet, colToSet);
                updateSubgrid(value, this.board[rowToSet][colToSet].subgrid, rowToSet, colToSet);
            }
            cellsInRowAllowingValue = 0;
        }
    }

    //Checks if only one cell in a subgrid allows a certain value - if so, set it
    public void checkSubgridExclusion(int subgrid){
        int cellsInRowAllowingValue = 0;
        int rowToSet = -1;
        int colToSet = -1;
        int subgridRowStart = 0;
        int subgridRowEnd = 0;
        int subgridColStart = 0;
        int subgridColEnd = 0;


        //Determine which partition of board to search based on subgrid
        switch (subgrid){
            case 1:
                subgridRowStart = 0;
                subgridRowEnd = 3;
                subgridColStart = 0;
                subgridColEnd = 3;
                break;
            case 2:
                subgridRowStart = 0;
                subgridRowEnd = 3;
                subgridColStart = 3;
                subgridColEnd = 6;
                break;
            case 3:
                subgridRowStart = 0;
                subgridRowEnd = 3;
                subgridColStart = 6;
                subgridColEnd = 9;
                break;
            case 4:
                subgridRowStart = 3;
                subgridRowEnd = 6;
                subgridColStart = 0;
                subgridColEnd = 3;
                break;
            case 5:
                subgridRowStart = 3;
                subgridRowEnd = 6;
                subgridColStart = 3;
                subgridColEnd = 6;
                break;
            case 6:
                subgridRowStart = 3;
                subgridRowEnd = 6;
                subgridColStart = 6;
                subgridColEnd = 9;
                break;
            case 7:
                subgridRowStart = 6;
                subgridRowEnd = 9;
                subgridColStart = 0;
                subgridColEnd = 3;
                break;
            case 8:
                subgridRowStart = 6;
                subgridRowEnd = 9;
                subgridColStart = 3;
                subgridColEnd = 6;
                break;
            case 9:
                subgridRowStart = 6;
                subgridRowEnd = 9;
                subgridColStart = 6;
                subgridColEnd = 9;
                int stfuintellij = 1;
                break;
        }



        for (int value = 1; value <= 9; value++) {                                          //for all possible values
            for(int r = subgridRowStart; r < subgridRowEnd; r++) {
                for (int c = subgridColStart; c < subgridColEnd; c++) {                     //scan each cell in subgrid
                    if(this.board[r][c].possibleValues[value-1] == value){                  //if cell allows "value" - note this
                        cellsInRowAllowingValue++;
                        rowToSet = r;
                        colToSet = c;
                    }
                }
            }


            //if there is only one cell in that row that allows "value", set it
            if(cellsInRowAllowingValue == 1){
                this.board[rowToSet][colToSet].value = value;
                updateRow(value, rowToSet, colToSet);
                updateColumn(value, rowToSet, colToSet);
                updateSubgrid(value, this.board[rowToSet][colToSet].subgrid, rowToSet, colToSet);
            }
            cellsInRowAllowingValue = 0;
        }
    }



    //Solution board -> all cells have list of possibilities of len 1
    public boolean isSolution(){
        for(int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if(this.board[row][col].numPossibilities != 1){
                    return false;
                }
            }
        }
        return true;
    }

    //Infeasible board -> at least one cell has a list of possibilities of length 0
    public boolean isInfeasible(){
        for(int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                //If there is an unset cell with no possibilities, board is impossible
                if (this.board[row][col].numPossibilities == 0) {
                    return true;
                }
            }
        }
        return false;
    }


    //return cell with smallest list of possible value
    public Cell determineCellToSearch(){
        int minPossibilities = 9;
        Cell cellToSearch = null;
        
        //Scan board
        for(int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                //If cell is fixed or already "determined to search", skip it
                if (this.board[row][col].value != 0 || this.board[row][col].searched == true || this.board[row][col].numPossibilities == 1)
                    continue;

                //find unfixed cell with min possibilities
                if (this.board[row][col].numPossibilities <= minPossibilities && this.board[row][col].value == 0) {
                    minPossibilities = this.board[row][col].numPossibilities;
                    cellToSearch = this.board[row][col];

                }
            }
        }

        if(cellToSearch != null) {
            cellToSearch.searched = true;
        }
        return cellToSearch;
    }
    
    //DEBUG: Printing board (includes row/col/subgrid divisions)
    public void printBoard(){
        for(int row = 0; row < 9; row++){
            for(int col = 0; col < 9; col++){
                if(this.board[row][col].value == 0){
                    System.out.print(" ");
                }else {
                    System.out.print(this.board[row][col].value);
                }
                if(col == 2 || col == 5){
                    System.out.print("  ║  ");
                }else{
                    System.out.print("  |  ");
                }
            }
            if(row == 2 || row == 5){
                System.out.print("\n════════════════════════════════════════════════════\n");
            }else {
                System.out.print("\n----------------------------------------------------\n");
            }
        }
        System.out.println("\n");
    }

    //DEBUG: Printing board
    public void printBoardSolution(){
        for(int row = 0; row < 9; row++){
            for(int col = 0; col < 9; col++){
                System.out.print(this.board[row][col].value + " ");
            }
            System.out.print("\n");
        }
        System.out.println("\n");
    }

    //DEBUG: printing data of a particular cell
    public void printCellDataInBoard() {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                this.board[row][col].printCellData();
            }
        }
    }
}
