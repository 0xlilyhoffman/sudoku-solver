import java.io.*;
import java.util.*;

public class Solver {

    //Tracks how many board configurations were tried
    public int nodesGenerated;

    public static void main(String[] args){
        Solver solver = new Solver();
        solver.nodesGenerated = 0;

        if(args.length != 2){
            System.out.println("Usage: java Solver infile.txt outfile.txt");
        }

        //Read infile to determine initial board configuration.
        String infile = args[0];
        int [][] initialCells = readInfile(infile);
        
        //Create board, fill in fixed cells, update board to reflect possible values for each cell
        Board board = new Board();
        board.initializeBoardConfiguration(initialCells);
        board.updateBoard();

        //Solve
        Board solution = solve(board, solver);
        
        //Print solution
        printOutfile(args[1], solution, solver);
    }

    //Sodoku solver - recursive backtracking solution
    public static Board solve(Board b, Solver s){
        s.nodesGenerated++;

        if(b.isSolution()){
            return b;
        }
        if(b.isInfeasible()){
            return null;
        }


        Cell c = b.determineCellToSearch();
        if(c == null){
            return null;
        }
        
        //For each possible value of the selected cell, set value of cell to possibility in that board copy and update rest of board accordingly
        for(int i = 0; i < c.possibleValues.length; i++){
            //Create copy of board
            Board bcopy = b.copyBoard(b);

            //If c.possibleValues[i] == 0, i is not a possible value for cell c
            if(c.possibleValues[i] == 0)
                continue;

            //Set cell value
            c.value = c.possibleValues[i];
            
            //Update copy of the board to reflect newly fixed cell
            bcopy.updateBoard();

            //Recurse - continue solving board bcopy assuming cell's "guessed value" is correct
            Board solution = solve(bcopy, s);

            //If correct solution found - return it
            if(solution != null) {
                return solution;
            }
        }
        
        //No solution found - return null - try again on different boad copy
        return null;
    }


    //Infile format: list of [row col value] for each fixed cell
    /* Eg
     * 1 4 5
     * 1 6 6
     * 2 3 4
     * ...
     */
    public static int[][] readInfile(String fileName){
        //Read infile one line at a time. Store each line in arraylist of strings
        String line = null;
        int nFixedCells = 0;
        ArrayList<String> fixedCellStringList = new ArrayList<>();
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);

            //Wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                fixedCellStringList.add(line);
                nFixedCells++;
            }

            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }

        //Convert arraylist of strings to int[][]
        int [][] fixedCellArray = new int[nFixedCells][3];
        for(int i = 0; i < nFixedCells; i++){
            fixedCellArray[i][0] = Character.getNumericValue(fixedCellStringList.get(i).charAt(0));
            fixedCellArray[i][1] = Character.getNumericValue(fixedCellStringList.get(i).charAt(2));
            fixedCellArray[i][2] = Character.getNumericValue(fixedCellStringList.get(i).charAt(4));
        }

        /*
        DEBUG: Print read infile
        for(int i = 0; i < nFixedCells; i++){
            for(int j = 0; j < 3; j++){
                System.out.print(fixedCellArray[i][j]);
            }
            System.out.print("\n");
        }
        */
        return fixedCellArray;

    }
    
    //Prints board solution to output file
    public static void printOutfile(String filename, Board board, Solver solver){
        try {
            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            
            if(board != null){
                for(int row = 0; row < 9; row++){
                    for(int col = 0; col < 9; col++){
                        writer.print(board.board[row][col].value + " ");
                    }
                    writer.print("\n");
                }
                writer.println("Nodes generated = " + solver.nodesGenerated);
            }else{
                writer.println("Infeasible");
                writer.println("Nodes generated = " + solver.nodesGenerated);
            }
            writer.close();
        }catch (Exception e){
            System.out.println("Error: " + e.getLocalizedMessage());
        }
    }
}
